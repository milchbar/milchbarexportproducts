<?php

use Doctrine\Common\Collections\ArrayCollection;
use Shopware\Commands\MilchbarExportProducts\StatusCommand;
use Shopware\Commands\MilchbarExportProducts\SecurityTokenCommand;
use Shopware\Components\MilchbarExportProducts\Utils\ExportProductsHelper;


class Shopware_Plugins_Backend_MilchbarExportProducts_Bootstrap extends Shopware_Components_Plugin_Bootstrap
{

	var $globaleStore;

    public function getVersion()
    {
        return '0.13.3';
    }

    public function getLabel()
    {
        return 'Export Products to JSON';
    }

    public function getInfo()
    {
        return array(
            'version' => $this->getVersion(),
            'label' => $this->getLabel(),
            'supplier' => 'Milchbar',
            'support' => 'Milchbar',
            'autor' => 'Milchbar',
            'copyright' => 'Copyright &copy; 2017, Milchbar',
            'link' => 'http://www.milchbar.nl'
        );
    }

    public function getCapabilities()
    {
        return array(
            'install' => true,
            'update' => true,
            'enable' => true
        );
    }

    public function install()
    {
        $this->registerEvents();
        $this->createConfig();

        return array('success' => true, 'invalidateCache' => array('backend'));
    }

    public function update($version)
    {
        $this->registerEvents();
        $this->createConfig();

        return array('success' => true, 'invalidateCache' => array('backend'));
    }

    public function uninstall()
    {
        return array('success' => true, 'invalidateCache' => array('backend'));
    }

    private function registerEvents()
    {
        $this->subscribeEvent(
            'Shopware_Console_Add_Command',
            'onAddConsoleCommand'
        );

        $this->subscribeEvent(
            'Enlight_Controller_Front_DispatchLoopStartup',
            'onStartDispatch'
        );

	$this->subscribeEvent(
            'Enlight_Controller_Action_PostDispatch_Backend_Base',
            'extendExtJS'
        );

    }


    public function extendExtJS(Enlight_Event_EventArgs $arguments)
    {


	$attributeService = Shopware()->Container()->get('shopware_attribute.crud_service');
        $attributes_columns = $attributeService->getList('s_articles_attributes');
        $attributes_columns_skip = [
            'id', 'articleID', 'articledetailsID',
            'attr1', 'attr2', 'attr3', 'attr4', 'attr5', 'attr6', 'attr7', 'attr8', 'attr9', 'attr10',
            'attr11', 'attr12', 'attr13', 'attr14', 'attr15', 'attr16', 'attr17', 'attr18', 'attr19', 'attr20',
        ];
        $articles_attributes_store = [];
        foreach ($attributes_columns as $attributes_column) {
          if (!in_array($attributes_column->getColumnName(), $attributes_columns_skip)) {
            $articles_attributes_store[] = [$attributes_column->getColumnName(), $attributes_column->getLabel()];
          }
        }

	$globaleStore = $articles_attributes_store;



        /** @var \Enlight_View_Default $view */
        //$view = $arguments->getSubject()->View();

        //$view->addTemplateDir($this->Path() . '/Views/');

        //$view->extendsTemplate('backend/MilchbarExportProducts/test.js');
    }



    /**
     * This callback function is triggered at the very beginning of the dispatch process and allows
     * us to register additional events on the fly. This way you won't ever need to reinstall you
     * plugin for new events - any event and hook can simply be registerend in the event subscribers
     */
    public function onStartDispatch(Enlight_Event_EventArgs $args)
    {
        $this->registerMyComponents();
        $this->registerHelperComponents();
        $this->registerMyTemplateDir();
        $this->registerMySnippets();


        $subscribers = array(
            new \Shopware\MilchbarExportProducts\Subscriber\ControllerPath(),
            new \Shopware\MilchbarExportProducts\Subscriber\Frontend()

        );

        foreach ($subscribers as $subscriber) {
            $this->Application()->Events()->addSubscriber($subscriber);
        }
    }

    /**
     * Registers the template directory, needed for templates in frontend an backend
     */
    public function registerMyTemplateDir()
    {
        Shopware()->Template()->addTemplateDir($this->Path() . 'Views');
    }

    /**
     * Registers the snippet directory, needed for backend snippets
     */
    public function registerMySnippets()
    {
        $this->Application()->Snippets()->addConfigDir(
            $this->Path() . 'Snippets/'
        );
    }

    public function registerHelperComponents()
    {
        $this->Application()->Loader()->registerNamespace(
            'Shopware\Components',
            $this->Path() . 'Components/'
        );
    }

    public function registerMyComponents()
    {
        $this->Application()->Loader()->registerNamespace(
            'Shopware\MilchbarExportProducts',
            $this->Path()
        );
    }


    /**
     * Register components directory
     */
    public function registerMyNamespace()
    {
      // $this->Application()->Loader()->registerNamespace('Shopware\Components', $this->Path() . 'Components/');
      $this->Application()->Loader()->registerNamespace('Shopware\Commands', $this->Path() . 'Commands/');
    }

    public function onAddConsoleCommand(Enlight_Event_EventArgs $args)
    {
        $this->registerMyNamespace();

        return new ArrayCollection(
            [
                new StatusCommand(),
                new SecurityTokenCommand(),
            ]
        );
    }

    private function createConfig()
    {
        $form = $this->Form();

        $form->setElement(
            'text',
            'milchbarExportProductsSecurityToken',
            [
                'label' => 'Security token',
                // 'value' => null
                'value' => password_hash(mt_rand(), PASSWORD_DEFAULT),
                'description' => 'When the plugin is reinstalled, this token also gets regenerated. So be sure to copy and save the token, so you can paste it back later. Or otherwise communicate the new token to the collecting party.'
            ]
        );

        $attributeService = Shopware()->Container()->get('shopware_attribute.crud_service');
        $attributes_columns = $attributeService->getList('s_articles_attributes');
        $attributes_columns_skip = [
            'id', 'articleID', 'articledetailsID',
            'attr1', 'attr2', 'attr3', 'attr4', 'attr5', 'attr6', 'attr7', 'attr8', 'attr9', 'attr10',
            'attr11', 'attr12', 'attr13', 'attr14', 'attr15', 'attr16', 'attr17', 'attr18', 'attr19', 'attr20',
        ];
        $articles_attributes_store = [];
        foreach ($attributes_columns as $attributes_column) {
          if (!in_array($attributes_column->getColumnName(), $attributes_columns_skip)) {
            $articles_attributes_store[] = [$attributes_column->getColumnName(), $attributes_column->getLabel()];
          }
        }

	$globaleStore = $articles_attributes_store;


        $form->setElement(
            'select',
            'milchbarExportProductsExtraAttributes',
            [
                'label' => 'Extra attributes',
		'store' => $globaleStore,
                //'store' => $articles_attributes_store,
                // 'store' => 'Shopware.apps.Attributes.store.Table',
                // 'store' => 'Shopware.apps.Attributes.store.DependingTable',
                // 'store' => 'Shopware.apps.Supplier.store.Supplier',
                // 'store' => 'Shopware.apps.SwagImportExport.store.Column',
                //'store' => 'Shopware.attribute.FieldHandlerInterface',
                // 'store' => 'Shopware.store.AttributeConfig',
                // 'store' => 'Shopware.attribute.ComboBoxFieldHandler',
		// 'store' => 'base.store.supplier',
                // 'value' => [],
                'description' => 'When the plugin is reinstalled, this multiselect also gets renewed. So only after reinstalling or updating the values are renewed with the actual attributes.',
                'multiSelect' => true
            ]
        );
    }

}
