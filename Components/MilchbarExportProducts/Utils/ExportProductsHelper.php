<?php

namespace Shopware\Components\MilchbarExportProducts\Utils;

use Shopware_Plugins_Backend_MilchbarExportProducts_Bootstrap as MilchbarExportProducts_Bootstrap;

class ExportProductsHelper
{
    protected $limit;
    protected $offset;
    protected $total;
    protected $time_start;

    /**
     * Construct
     *
     * @param array $data
     * @throws \Exception
     */
    public function __construct(array $data)
    {
        if (isset($data['limit'])) {
            $this->limit = $data['limit'];
        }
        if (isset($data['offset'])) {
            $this->offset = $data['offset'];
        }
        $this->time_start = microtime(true);
    }

    /**
     * Prepares export
     *
     * @return array
     */
    public function prepareExport()
    {
        $output = "prepareExport\n";

        $output .= $this->processedTime()."\n";

        $shop = Shopware()->Shop();
        $output .= "shop: ".$shop->getName()." (id:".$shop->getId().")\n";

        $milchbarExportProductsExtraAttributes = Shopware()->Plugins()->Backend()->MilchbarExportProducts()->Config()->get('milchbarExportProductsExtraAttributes');
        if ($milchbarExportProductsExtraAttributes !== NULL) {
          $articles_extra_attributes = $milchbarExportProductsExtraAttributes->toArray();
        }

        $_limit = intval($this->limit) > 0 ? intval($this->limit) : null;
        $_offset = intval($this->offset) > 0 ? intval($this->offset) : null;
        $articleDetailsRepo = Shopware()->Models()->getRepository('Shopware\Models\Article\Detail');
        $articleDetails = $articleDetailsRepo->findBy(array('active' => 1), array('number' => 'ASC'), $_limit, $_offset);
        $this->total = count($articleDetails);

        $output .= $this->processedTime()."\n";

        $sql = "SELECT * FROM `s_core_rewrite_urls`
                WHERE `s_core_rewrite_urls`.`main` = ?
                AND `s_core_rewrite_urls`.`org_path` LIKE ?
                AND `s_core_rewrite_urls`.`subshopID` = ?";
        $core_rewrite_urls = Shopware()->Db()->fetchAll($sql, array(1, 'sViewport=detail&sArticle=%', 1));

        $output .= $this->processedTime()."\n";

        $products = array();
        foreach ($articleDetails as $article_detail) {
          $article = $article_detail->getArticle();

          $url_key = array_search('sViewport=detail&sArticle='.$article->getId(), array_column($core_rewrite_urls, 'org_path'));

          $articles_attribute = $article_detail->getAttribute();

          $article_tax = $article->getTax();

          $article_images = $article->getImages();
          $_article_images = [];
          if (count($article_images) > 0) {
            $mediaService = Shopware()->Container()->get('shopware_media.media_service');
            foreach ($article_images as $article_image) {
              $article_image_media = $article_image->getMedia();
              if ($article_image_media !== null) {
                try {
                    $_article_images[] = $mediaService->getUrl($article_image_media->getPath());
                } catch (\Doctrine\ORM\EntityNotFoundException $e) { $output .=  "Exception Found for ".$article_detail->getNumber().": ".$e->getMessage()."\n"; }
              }
            }
          }

          $article_supplier = $article->getSupplier();
          if ($article_supplier !== null) {
            try {
              $_article_supplier_name = $article_supplier->getName();
            } catch (\Doctrine\ORM\EntityNotFoundException $e) { $output .=  "Exception Found for ".$article_detail->getNumber().": ".$e->getMessage()."\n"; }
          }

          $article_categories = $article->getCategories();
          $_article_categories = [];
          if (count($article_categories) > 0) {
            foreach ($article_categories as $article_category) {
              if ($article_category !== null) {
                try {
                  $_article_category_name = $article_category->getName();
                  $_article_categories[] = $article_category->getName();
                } catch (\Doctrine\ORM\EntityNotFoundException $e) { $output .=  "Exception Found for ".$article_detail->getNumber().": ".$e->getMessage()."\n"; }
              }
            }
          }

          $article_seo_categories = $article->getSeoCategories();
          $_article_seo_categories = [];
          if (count($article_seo_categories) > 0) {
            foreach ($article_seo_categories as $article_seo_category) {
              if ($article_seo_category !== null) {
                try {
                  $article_category = $article_seo_category->getCategory();
                  $_article_seo_categories[] = $article_category->getName();
                } catch (\Doctrine\ORM\EntityNotFoundException $e) { $output .=  "Exception Found for ".$article_detail->getNumber().": ".$e->getMessage()."\n"; }
              }
            }
          }

          $_article_detail_configurator_options_has_options = false;
          $article_detail_configurator_options = $article_detail->getConfiguratorOptions();
          if (count($article_detail_configurator_options) > 0) {
            $_article_detail_configurator_options_has_options = true;
          } else {
            $_article_detail_configurator_options_has_options = false;
          }

          $_article_details_unit_unit = null;
          $_article_details_unit_name = null;
          $article_details_unit = $article_detail->getUnit();
          if ($article_details_unit !== null) {
            try {
              $_article_details_unit_unit = $article_details_unit->getUnit();
              $_article_details_unit_name = $article_details_unit->getName();
            } catch (\Doctrine\ORM\EntityNotFoundException $e) { $output .=  "Exception Found for ".$article_detail->getNumber().": ".$e->getMessage()."\n"; }
          }

          if ($_article_detail_configurator_options_has_options) {
            $_article_detail_url = (isset($_SERVER['HTTPS']) ? 'https' : 'http').'://'.$_SERVER['HTTP_HOST'].'/'.$core_rewrite_urls[$url_key]['path'].'?number='.$article_detail->getNumber();
          } else {
            $_article_detail_url = (isset($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/' . $core_rewrite_urls[$url_key]['path'];
          }

          $shop_customer_group = $shop->getCustomerGroup();
          $article_detail_prices = $article_detail->getPrices();
          $_article_detail_price = null;
          $_article_detail_pseudo_price = 0;
          $_article_detail_pseudo_price_ek = 0;
          $_article_pseudo_price = 0;
          foreach ($article_detail_prices as $article_detail_price) {
            // $output .= $article_detail->getNumber()." - ".$article_detail_price->getPrice()."\n";
            // $output .= $article_detail->getNumber()." - ".$article_detail_price->getPseudoPrice()."\n";
            $article_details_price_group = $article_detail_price->getCustomerGroup();
            if ($article_details_price_group->getKey() == $shop_customer_group->getKey()) {
              $_article_detail_price = $article_detail_price->getPrice();
              if ($article_detail_price->getPseudoPrice() > 0) {
                $_article_detail_pseudo_price = $article_detail_price->getPseudoPrice();
              }
            }
            if ($article_details_price_group->getKey() == 'EK') {
              $_article_detail_price_ek = $article_detail_price->getPrice();
              if ($article_detail_price->getPseudoPrice() > 0) {
                $_article_detail_pseudo_price_ek = $article_detail_price->getPseudoPrice();
              }
            }
          }
          if ($_article_detail_price === null) {
            $_article_detail_price = $_article_detail_price_ek;
            if ($_article_detail_pseudo_price_ek > 0) {
              $_article_detail_pseudo_price = $_article_detail_pseudo_price_ek;
            }
          }
          if ($shop_customer_group->getTax()) {
            if ($_article_detail_price > 0 && $article_tax->getTax() > 0) {
              $_article_price = str_replace('.', ',', round(round($_article_detail_price, 2) / 100 * (100 + $article_tax->getTax()), 2));
              if ($_article_detail_pseudo_price > 0) {
                $_article_pseudo_price = str_replace('.', ',', round(round($_article_detail_pseudo_price, 2) / 100 * (100 + $article_tax->getTax()), 2));
              }
            } else {
              $_article_price = str_replace('.', ',', round($_article_detail_price, 2));
              if ($_article_detail_pseudo_price > 0) {
                $_article_pseudo_price = str_replace('.', ',', round($_article_detail_pseudo_price, 2));
              }
            }
          } else {
            $_article_price = str_replace('.', ',', round($_article_detail_price, 2));
            if ($_article_detail_pseudo_price > 0) {
              $_article_pseudo_price = str_replace('.', ',', round($_article_detail_pseudo_price, 2));
            }
          }

          $product = array(
            'id' => $article_detail->getId(),
            'parent_id' => $article->getId(),
            'articlenumber' => $article_detail->getNumber(),
            'name' => $article->getName(),
            'description' => $article->getDescription(),
            'description_long' => $article->getDescriptionLong(),
            'url' => $_article_detail_url,
            'images' => $_article_images,
            'ean' => $article_detail->getEan(),
            'active' => $article_detail->getActive(),
            'stock' => $article_detail->getInStock(),
            'price' => $_article_price,
            'pseudo_price' => $_article_pseudo_price,
            'shippingtime' => $article_detail->getShippingTime(),
            'minpurchase' => $article_detail->getMinPurchase(),
            'unit' => $_article_details_unit_unit,
            'unitname' => $_article_details_unit_name,
            'purchaseunit' => $article_detail->getPurchaseUnit(),
            'referenceunit' => $article_detail->getReferenceUnit(),
            'brand' => $_article_supplier_name,
            'category' => $_article_category_name,
            'categories' => $_article_categories,
            'seo_categories' => $_article_seo_categories,
            'attr1' => $articles_attribute->getAttr1(),
            'attr2' => $articles_attribute->getAttr2(),
            'attr3' => $articles_attribute->getAttr3(),
            'attr4' => $articles_attribute->getAttr4(),
            'attr5' => $articles_attribute->getAttr5(),
            'attr6' => $articles_attribute->getAttr6(),
            'attr7' => $articles_attribute->getAttr7(),
            'attr8' => $articles_attribute->getAttr8(),
            'attr9' => $articles_attribute->getAttr9(),
            'attr10' => $articles_attribute->getAttr10(),
            'attr11' => $articles_attribute->getAttr11(),
            'attr12' => $articles_attribute->getAttr12(),
            'attr13' => $articles_attribute->getAttr13(),
            'attr14' => $articles_attribute->getAttr14(),
            'attr15' => $articles_attribute->getAttr15(),
            'attr16' => $articles_attribute->getAttr16(),
            'attr17' => $articles_attribute->getAttr17(),
            'attr18' => $articles_attribute->getAttr18(),
            'attr19' => $articles_attribute->getAttr19(),
            'attr20' => $articles_attribute->getAttr20(),
          );
          if (count($articles_extra_attributes) > 0) {
            foreach ($articles_extra_attributes as $articles_extra_attribute) {
              $attributeGetter = [];
              $attributeGetter[] = 'get';
              $articles_extra_attribute_a = explode('_', $articles_extra_attribute);
              foreach ($articles_extra_attribute_a as $_articles_extra_attribute_a) {
                $_articles_extra_attribute_a_first_char = substr($_articles_extra_attribute_a, 0, 1);
                if (is_numeric($_articles_extra_attribute_a_first_char)) {
                  $attributeGetter[] = '_'.$_articles_extra_attribute_a;
                } else if (is_string($_articles_extra_attribute_a_first_char)) {
                  $attribute_uppercase_words = ucfirst($_articles_extra_attribute_a);
                  $attributeGetter[] = $attribute_uppercase_words;
                }
              }
              $attributeGetterFunction = implode($attributeGetter);
              if (is_callable(array($articles_attribute, $attributeGetterFunction))) {
                $product[$articles_extra_attribute] = $articles_attribute->$attributeGetterFunction();
              }
            }
          }
          if ($_article_detail_configurator_options_has_options) {
            foreach ($article_detail_configurator_options as $article_detail_configurator_option) {
              $article_detail_configurator_option_group = $article_detail_configurator_option->getGroup();
              $_article_detail_configurator_option_group_name = str_replace(' ', '_', strtolower($article_detail_configurator_option_group->getName()));
              $_article_detail_configurator_option_name = $article_detail_configurator_option->getName();
              $product[$_article_detail_configurator_option_group_name] = $_article_detail_configurator_option_name;
            }
          }

          $products[] = $product;
        }

        $output .= $this->processedTime()."\n";

        Shopware()->PluginLogger()->info("ExportProductsHelper\\prepareExport()", [$output]);

        return array('success' => true, 'output' => $output, 'products' => $products);
    }

    /**
     * Executes export
     *
     * @return array
     */
    public function executeExport($products)
    {
        $output = "executeExport\n";

        $output .= $this->processedTime()."\n";

        $_limit = intval($this->limit) > 0 ? intval($this->limit) : 0;
        $_offset = intval($this->offset) > 0 ? intval($this->offset) : 0;
        $_total = intval($this->total) > 0 ? intval($this->total) : 0;

        $_products = $products;

        $oExportProducts = new MilchbarExportProducts_Bootstrap();

        $feed = array(
          'config' => array(
            'system' => 'Shopware',
            'extension' => 'MilchbarExportProducts',
            'extension_version' => $oExportProducts->getVersion(),
            'url' => (isset($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'],
            'products_total' => $_total,
            'products_start' => $_offset,
            'products_limit' => $_limit,
            'processing_time' => $this->processedTime($time_start),
          ),
          'products' => $_products
        );

        $output .= $this->processedTime()."\n";

        Shopware()->PluginLogger()->info("ExportProductsHelper\\executeExport()", [$output]);

        return array('success' => true, 'output' => $output, 'feed' => $feed);
    }

    protected function processedTime()
    {
        $time_interval = microtime(true);
        $time_difference = $time_interval - $this->time_start;
        return number_format($time_difference, 2);
    }

}
