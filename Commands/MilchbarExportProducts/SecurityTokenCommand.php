<?php

namespace Shopware\Commands\MilchbarExportProducts;

use Shopware\Commands\ShopwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Shopware\Components\MilchbarExportProducts\Utils\ExportProductsHelper;

class SecurityTokenCommand extends ShopwareCommand
{

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('milchbar:exportproducts:token')
            ->setDescription('Add or change the security token')
            ->setHelp("The <info>%command.name%</info> will add or change the security token.");
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
          $this->start($output);
    }

    /**
     *
     * @param OutputInterface $output
     */
    protected function start(OutputInterface $output)
    {
        $milchbarExportProductsSecurityToken = Shopware()->Plugins()->Backend()->MilchbarExportProducts()->Config()->get('milchbarExportProductsSecurityToken');
        if ($milchbarExportProductsSecurityToken != '') {
          $output->writeln('<info>Security token: '.$milchbarExportProductsSecurityToken.'</info>');
        } else {
          $output->writeln('<info>Security token: missing</info>');
          $milchbarExportProductsSecurityToken = password_hash(mt_rand(), PASSWORD_DEFAULT);
          $output->writeln('<info>Generating token: '.$milchbarExportProductsSecurityToken.'</info>');
          $shop = Shopware()->Models()->getRepository('Shopware\Models\Shop\Shop')->findOneBy(array('default' => true));
          $pluginManager  = Shopware()->Container()->get('shopware_plugininstaller.plugin_manager');
          $plugin = $pluginManager->getPluginByName('MilchbarExportProducts');
          $pluginManager->saveConfigElement($plugin, 'milchbarExportProductsSecurityToken', $milchbarExportProductsSecurityToken, $shop);
        }
    }

}
