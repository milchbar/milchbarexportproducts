<?php

use Shopware\Components\MilchbarExportProducts\Utils\ExportProductsHelper;


/**
 * Frontend controller
 */
class Shopware_Controllers_Frontend_MilchbarExportProducts extends Enlight_Controller_Action
{


    public function init()
    {
        Shopware()->Plugins()->Controller()->ViewRenderer()->setNoRender();
    }

    public function indexAction()
    {
        $request = $this->Request();
    }
    public function feedAction()
    {
      $request = $this->Request();
      $_debug = $request->getParam('debug') ? true : false;

      if ($_debug) {
        echo "<pre>";
      }

      if ($request->getParam('token')) {
        $_sec_token = $request->getParam('token');
        $milchbarExportProductsSecurityToken = Shopware()->Plugins()->Backend()->MilchbarExportProducts()->Config()->get('milchbarExportProductsSecurityToken');
        if ($_sec_token === $milchbarExportProductsSecurityToken) {
        } else {
          die("token invalid");
        }
      } else {
        die("token missing");
      }

      $_offset = $request->getParam('start') != '' ? str_replace(' ', '-', $request->getParam('start')) : 'no-offset';
      $_limit = $request->getParam('limit') != '' ? str_replace(' ', '-', $request->getParam('limit')) : 'no-limit';

      $exportProductsHelper = new ExportProductsHelper(
            [
                'offset' => $_offset,
                'limit' => $_limit,
            ]
      );

      $return = $exportProductsHelper->prepareExport();
      if ($_debug) {
        echo $return['output'];
      }

      $return = $exportProductsHelper->executeExport($return['products']);
      if ($_debug) {
        echo $return['output'];
      }

      echo json_encode($return['feed']);
    }

}
